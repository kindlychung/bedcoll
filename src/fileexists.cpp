#include "fileexists.h"


bool fileExists(const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

void fileExists(const std::string& name, bool exitIfNotExist, bool exitIfExist) {
    bool existStatus = fileExists(name);
    if(not existStatus) {
        if(exitIfNotExist) {
            std::cerr << "File " << name << " does not exist!\n";
            exit(1);
        }
    }
    else {
        if(exitIfExist) {
            std::cerr << "File " << name << " already exists!\n";
            exit(1);
        }
    }
}

bool fileExists(const std::string& name, bool rmIfExist) {
    bool existStatus = fileExists(name);
    if(existStatus) {
        if(rmIfExist) {
#ifdef bedcolldebug
            std::cout << "Removing exsiting file: " << name << "\n";
#endif
            if( remove(name.c_str()) != 0 ) {
                std::cerr << "Failed to remove " << name << "!\n";
                exit(1);
            }
        }
    }
    return existStatus;
}
