#ifndef FILEEXISTS_H
#define FILEEXISTS_H

#include <string>
#include <iostream>

bool fileExists(const std::string& name);
void fileExists(const std::string& name, bool exitIfNotExist, bool exitIfExist);
bool fileExists(const std::string& name, bool rmIfExist);

//bool fileExists(char* name);
//void fileExists(char *name, bool exitIfNotExist, bool exitIfExist);

#endif // FILEEXISTS_H
