#!/usr/bin/env bash

CFLAGS='-O3 -std=c++11'
linkflags='-L/usr/local/lib -lboost_system -lboost_filesystem -lboost_program_options'

rm -rf obj
mkdir obj
for srcfile in src/*.cpp; do
    outfile=$(basename $srcfile)
    outfile=${outfile%.cpp}.o
    outfile="obj/$outfile"
    echo Building $outfile ...
    g++ $srcfile $CFLAGS -c -o $outfile
done
echo Linking...
g++ obj/*.o $CFLAGS $linkflags -o bedcoll
